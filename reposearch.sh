#!/bin/bash

fedora_gems_raw=$PWD/rubygems_fedora_raw
fedora_gems=$PWD/rubygems_fedora
bugzilla_gems_raw=$PWD/rubygems_bugzilla_raw
bugzilla_gems=$PWD/rubygems_bugzilla

echo 'Searching Fedora repositories...'
dnf repoquery rubygem-* > $fedora_gems_raw

# Striping uneeded symbols and the rubygem- prefix
cat $fedora_gems_raw | sed -e 's/rubygem-//g;s/.fc23.noarch//g;s/.fc23.x86_64//g;/-doc/d;/-devel/d;/-debuginfo/d;/Last metadata/d' | awk 'BEGIN { FS=":" }; {print $1}' | sed 's/.\{2\}$//g' | sort -ru > $fedora_gems

echo 'Done!'

# Install python-bugzilla first
echo 'Searching Bugzilla for Review Requests...'
# Save raw query
bugzilla query --product=Fedora --bug_status=NEW,ASSIGNED --component='Package Review' --short_desc='rubygem-' | sort -k2 -r > $bugzilla_gems_raw

# Keep names only
bugzilla query --product=Fedora --bug_status=NEW,ASSIGNED --component='Package Review' --short_desc='rubygem-' \
  | awk 'BEGIN { FS = " - " } ; { print $3 }' | awk 'BEGIN { FS = ":" } ; { print $2 }' | sed -e 's/ rubygem-//' \
  | sort -k1 -u > $bugzilla_gems

echo 'Done!'
